import UserList from '../core/Public/UserList/user';
import AddUser from '../core/Public/AddUser/addUser';
import Home from '../components/React/Home';
// import Edit from '../Components/React/Edit';

export const appRoutes: CustomRoute[] = [
    {
        path: "/users",
        component: UserList,
    },
    {
        path: "/add",
        component: AddUser,
    },
    {
        path: "/",
        component: Home,
    }
]