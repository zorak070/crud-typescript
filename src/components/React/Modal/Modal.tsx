import React from 'react';
import { Modal, ModalBody, Button, ModalFooter } from 'reactstrap';


const Modals = (props: any) => {
    const { open, close, confirm, content, toggle } = props;
    return (
        <Modal isOpen={open} toggle={toggle} onExit={close} >
            <ModalBody>
                {content}
            </ModalBody>
            <ModalFooter>
                <Button color="primary" onClick={confirm}>Yes</Button>{' '}
                <Button color="secondary" onClick={close}>No</Button>
            </ModalFooter>
        </Modal>
    )
}


export default Modals;