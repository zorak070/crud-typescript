import React from 'react';
import './App.css';
import { Link } from 'react-router-dom';
import { appRoutes } from './routes';
import PublicRoute from './routes/Public/PublicRoute';
import { Nav, NavItem, NavLink } from 'reactstrap';

function App() {
  return (
    <div className="App">
      <Nav>
        <NavItem>
          <NavLink tag={Link} to="/" activeclassname="active">
            Home
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink tag={Link} to="/users" activeclassname="active">
            User List
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink tag={Link} to={'/add'} activeclassname="active">
            Add emp
              </NavLink>
        </NavItem>
      </Nav>
      <PublicRoute
        appRoutes={appRoutes}
        redirectPath={[{ from: "*", to: "/" }]}
      />
    </div>
  );
}

export default App;
