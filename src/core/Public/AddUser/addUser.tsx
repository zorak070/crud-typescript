import React, { useCallback } from 'react'
import { connect, ConnectedProps } from 'react-redux'
import { RootState } from '../../../store/rootReducer';
import { saveUser } from '../../../store/modules/user/saveUser'
import { useHistory } from 'react-router-dom';
import useForm from './useForm';
import validateInfo from './validator'

export interface UserCredentials { name: string; address: string, phoneNumber: string, email: string, gender: string, dateOfBirth: string }
interface ProductProps extends PropsFromRedux { }

const AddUser = (props: ProductProps) => {

    const { saveUser } = props;
    const history = useHistory();
    const saveData = useCallback(async (user: UserCredentials) => {
        const res = await saveUser(user);
        if (res.message === "Saved Successfully") {
            history.push("/users");
        }
    }, [saveUser, history])
    const { handleChange, values, handleSubmit, errors } = useForm(validateInfo, saveData);


    return (
        <div className="container">
            <div className="w-75 mx-auto shadow p-3">
                <h2 className="text-center mb-2">Add A User</h2>
                <form onSubmit={handleSubmit}>
                    <div className="form-group m-3">
                        <label>Name</label>
                        <input type="text"
                            className="form-control"
                            placeholder="Enter Your Name"
                            name="name"
                            value={values.name}
                            onChange={handleChange} />
                        {errors.name && <p>{errors.name}</p>}
                    </div>
                    <div className="form-group m-3">
                        <label>Address</label>
                        <input type="address"
                            className="form-control"
                            placeholder="Enter Your Address"
                            name="address"
                            value={values.address}
                            onChange={handleChange} />
                        {errors.address && <p>{errors.address}</p>}
                    </div>
                    <div className="form-group m-3">
                        <label>Phone</label>
                        <input type="number"
                            className="form-control"
                            placeholder="Enter Your phone"
                            name="phoneNumber"
                            value={values.phoneNumber}
                            onChange={handleChange} />
                        {errors.phoneNumber && <p>{errors.phoneNumber}</p>}
                    </div>
                    <div className="form-group m-3">
                        <label>Email</label>
                        <input type="email"
                            className="form-control"
                            placeholder="Enter Your email"
                            name="email"
                            value={values.email}
                            onChange={handleChange} />
                        {errors.email && <p>{errors.email}</p>}
                    </div>
                    <div className='form-group m-3'>
                        <label>Gender</label>
                        <div className="radio-buttons mx-2">
                            Male
                            <input type="radio"
                                className=" mx-2"
                                name="gender"
                                value="Male"
                                checked={values.gender === "Male"}
                                onChange={handleChange} />
                          Female
                          <input type="radio"
                                className=" mx-2"
                                name="gender"
                                value="Female"
                                checked={values.gender === "Female"}
                                onChange={handleChange} />
                        </div>
                    </div>
                    <div className="form-group m-3">
                        <label>DOB</label>
                        <input type="date"
                            className="form-control"
                            placeholder="Enter Your DOB"
                            name="dateOfBirth"
                            value={values.dateOfBirth}
                            onChange={handleChange}
                        />
                        {errors.dateOfBirth && <p>{errors.dateOfBirth}</p>}
                    </div>
                    <button type="submit" className="btn btn-primary " >
                        Add User
                </button>
                </form>
            </div>
        </div>
    );
};

const mapStateToProps = (state: RootState) => ({
    saveUserData: state.user.saveUser
})

const mapDispatchToProps = {
    saveUser
}

const connector = connect(mapStateToProps, mapDispatchToProps);
type PropsFromRedux = ConnectedProps<typeof connector>

export default connector<any>(AddUser)