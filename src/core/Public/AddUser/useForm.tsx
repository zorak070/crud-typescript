import { useState, useEffect } from 'react';

const useForm = (validate: any, saveUser: any) => {
    const [values, setValues] = useState({
        name: "",
        address: "",
        phoneNumber: "",
        email: "",
        gender: "Male",
        dateOfBirth: ""
    });
    const [errors, setErrors] = useState({
        name: "",
        address: "",
        phoneNumber: "",
        email: "",
        gender: "",
        dateOfBirth: ""
    });
    const [isSubmitting, setIsSubmitting] = useState(false);

    const handleChange = (e: any) => {
        const { name, value } = e.target;
        setValues({
            ...values,
            [name]: value
        });
    };

    const handleSubmit = (e: any) => {
        e.preventDefault();
        setErrors(validate(values));
        setIsSubmitting(true);
    };

    useEffect(
        () => {
            if (Object.keys(errors).length === 0 && isSubmitting) {
                saveUser(values);
            }
        },
        [errors]
    );

    return { handleChange, handleSubmit, values, errors };
};

export default useForm;