export default function validateInfo(values: any) {
    let errors: any = {};
    if (!values.name.trim()) {
        errors.name = 'name required';
    }

    if (!values.address.trim()) {
        errors.address = 'Address required';
    }
    if (!values.dateOfBirth.trim()) {
        errors.dateOfBirth = 'DOB required';
    }
    console.log(values.phoneNumber.trim(), "trim")
    if (!values.phoneNumber.trim()) {
        errors.phoneNumber = 'Phone required';
    } else if (!/^\d{10}$/.test(values.phoneNumber)) {
        errors.phoneNumber = 'Phone number is invalid';
    }

    if (!values.email) {
        errors.email = 'Email required';
    } else if (!/\S+@\S+\.\S+/.test(values.email)) {
        errors.email = 'Email address is invalid';
    }

    return errors;
}