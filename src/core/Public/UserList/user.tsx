import React, { useEffect, useCallback, useState } from 'react'
import { connect, ConnectedProps } from 'react-redux'
import { getAllUser } from '../../../store/modules/user/getUser'
import { deleteUser } from '../../../store/modules/user/deleteUser'
import { RootState } from '../../../store/rootReducer';
import { Table, Button } from 'reactstrap';
import Modal from '../../../components/React/Modal/Modal'

export interface recievedData {
    id: number
    name: string
    address: string
    dateOfBirth: string
    gender: string
    email: string
    phoneNumber: string
    educationDetails: string[]
}

interface ProductProps extends PropsFromRedux { }

const UserList = (props: ProductProps) => {
    const { getAllUser, deleteUser, getAllUserData } = props;

    const [userList, setUserList] = useState([] as any);
    const [isOpen, setIsOpen] = useState(false as boolean);
    const [deleteId, setId] = useState(0 as number);
    const toggle = () => setIsOpen(!isOpen);

    const fetchAllUserData = useCallback(async () => {
        const data = await getAllUser();
        setUserList(data);
    }, [getAllUser])

    const deleteByID = useCallback(async (id: number) => {
        await deleteUser(id);
        const setUserRes = await getAllUser();
        setUserList(setUserRes);
    }, [deleteUser, getAllUser])

    useEffect(() => {
        fetchAllUserData()
    }, [fetchAllUserData])

    if (getAllUserData.isFetching) {
        return <p>Loading...</p>
    }

    return (
        <div>
            {isOpen && <Modal open={isOpen} close={toggle} toggle={toggle} confirm={() => deleteByID(deleteId)}
                content="Confirm" />}
            <Table striped bordered>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Address</th>
                        {/* <th>DOB</th> */}
                        <th>Gender</th>
                        <th>Email</th>
                        <th>Phone Number</th>
                    </tr>
                </thead>
                <tbody>
                    {userList?.list?.length > 0 && userList.list.slice(-10).map((user: recievedData, index: number) => (
                        <tr key={user.id}>
                            <th scope="row">{user.id}</th>
                            <td>{user.name}</td>
                            <td>{user.address}</td>
                            {/* <td>{user.dateOfBirth}</td> */}
                            <td>{user.gender}</td>
                            <td>{user.email}</td>
                            <td>{user.phoneNumber}</td>
                            <td><Button outline color="danger" onClick={() => { toggle(); setId(user.id) }}>Delete</Button></td>
                        </tr>
                    ))}
                </tbody>
            </Table>
        </div>
    )
}

const mapStateToProps = (state: RootState) => ({
    getAllUserData: state.user.getAllUserData
})

const mapDispatchToProps = {
    getAllUser,
    deleteUser
}

const connector = connect(mapStateToProps, mapDispatchToProps);
type PropsFromRedux = ConnectedProps<typeof connector>

export default connector<any>(UserList)