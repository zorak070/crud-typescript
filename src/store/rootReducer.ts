import { AnyAction, combineReducers } from "redux";

import userReducer from "./modules/user";

export const appReducer = combineReducers({
    user: userReducer
});

export type RootState = ReturnType<typeof appReducer>;
type TState = ReturnType<typeof appReducer> | undefined;

export default function rootReducer(state: TState, action: AnyAction) {
    // if (action.type === "USER_LOG_OUT") {
    //     state = undefined;
    //     try {
    //     } catch (err) {
    //         console.error("Logout Error", err);
    //     }
    // }

    return appReducer(state, action);
};