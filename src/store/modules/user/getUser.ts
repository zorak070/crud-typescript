import { Dispatch } from "redux";
import { AppThunk } from "../../index";
import { apiList } from "../../actionNames/index";
import initDefaultAction, { APIResponseDetail } from "../../helper/default-action";
import initDefaultReducer from "../../helper/default-reducer";
import initialState from "../../helper/default-state";

type UserData = {
    status: string,
    list: {
        id: number,
        name: string,
        address: string,
        dateOfBirth: string,
        gender: string,
        email: string,
        phoneNumber: string,
        educationDetails: string[]
    }[]

}

const apiDetails = Object.freeze(apiList.user.getAll);


export default function getAllUserReducer(state = initialState, action: DefaultAction): DefaultState<UserData> {
    const stateCopy = Object.assign({}, state);
    const actionName = apiDetails.actionName;

    return initDefaultReducer(actionName, action, stateCopy);
}

export const getAllUser = (): AppThunk<APIResponseDetail<UserData>> => async (dispatch: Dispatch) => {

    return await initDefaultAction(apiDetails, dispatch, { disableSuccessToast: false });
};