import { Dispatch } from "redux";
import { AppThunk } from "../../index";
import { apiList } from "../../actionNames/index";
import initDefaultAction, { APIResponseDetail } from "../../helper/default-action";
import initDefaultReducer from "../../helper/default-reducer";
import initialState from "../../helper/default-state";
import { UserCredentials } from '../../../core/Public/AddUser/addUser';


const apiDetails = Object.freeze(apiList.user.save);


export default function saveUserReducer(state = initialState, action: DefaultAction): DefaultState<UserData> {
    const stateCopy = Object.assign({}, state);
    const actionName = apiDetails.actionName;

    return initDefaultReducer(actionName, action, stateCopy);
}

export const saveUser = (requestData: UserCredentials): AppThunk<APIResponseDetail<UserData>> => async (dispatch: Dispatch) => {
    console.log("user called")
    return await initDefaultAction(apiDetails, dispatch, { requestData, disableSuccessToast: false, requestMethod: "POST" });
};