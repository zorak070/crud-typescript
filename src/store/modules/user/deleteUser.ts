import { Dispatch } from "redux";
import { AppThunk } from "../../index";
import { apiList } from "../../actionNames/index";
import initDefaultAction, { APIResponseDetail } from "../../helper/default-action";
import initDefaultReducer from "../../helper/default-reducer";
import initialState from "../../helper/default-state";

const apiDetails = Object.freeze(apiList.user.delete);



export default function deleteUserReducer(state = initialState, action: DefaultAction): DefaultState<UserData> {
    const stateCopy = Object.assign({}, state);
    const actionName = apiDetails.actionName;

    return initDefaultReducer(actionName, action, stateCopy);
}

export const deleteUser = (id: number): AppThunk<APIResponseDetail<UserData>> => async (dispatch: Dispatch) => {
    let newApi = {
        ...apiDetails,
        controllerName: apiDetails.controllerName.replace("{empolyeeID}", `${id}`)
    };
    return await initDefaultAction(newApi, dispatch, { disableSuccessToast: false });
};