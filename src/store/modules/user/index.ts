import { combineReducers } from 'redux';
import getAllUserReducer from './getUser';
import deleteUserReducer from './deleteUser';
import saveUserReducer from './saveUser';

const shoppingReducer = combineReducers({
    getAllUserData: getAllUserReducer,
    deleteUser: deleteUserReducer,
    saveUser: saveUserReducer
})

export default shoppingReducer;