export enum RequestMethod {
    GET = "GET",
    DELETE = "DELETE",
    HEAD = "HEAD",
    OPTIONS = "OPTIONS",
    POST = "POST",
    PUT = "PUT",
    PATCH = "PATCH",
    PURGE = "PURGE",
    LINK = "LINK",
    UNLINK = "UNLINK"
}

export enum RequestBodyType {
    /**If request id in application/x-www-form-urlencoded as string*/
    QUERYSTRING = "QUERY-STRING",
    /**If request is in formdata*/
    FORMDATA = "FORM-DATA",
    /**If request requires Bearer*/
    AUTH = "AUTH",
    /**If request is open*/
    NOAUTH = "NO-AUTH"
}

/**
 * API detail with redux action associated with it
 */
export interface apiDetailType {
    /**Redux Action Name */
    actionName: string;
    /**Request API URI */
    controllerName: string;
    /**Request Method; Defaults as GET */
    requestMethod?: RequestMethod
    /**Request Body Type */
    requestBodyType?: RequestBodyType;
};


const apiDetails = {

    user: {
        getAll: {
            controllerName: "http://173.249.45.237:8081/hrs/employee",
            actionName: "GET_ALL",
            requestMethod: RequestMethod.GET
        },
        getOne: {
            controllerName: "http://173.249.45.237:8081/hrs/employee/{empolyeeID}",
            actionName: "GET_ONE",
            requestMethod: RequestMethod.GET
        },
        save: {
            controllerName: "http://173.249.45.237:8081/hrs/employee/save",
            actionName: "SAVE",
            requestMethod: RequestMethod.POST,
            requestBodyType: RequestBodyType.NOAUTH
        },
        delete: {
            controllerName: "http://173.249.45.237:8081/hrs/employee/{empolyeeID}",
            actionName: "DELETE",
            requestMethod: RequestMethod.DELETE
        }
    }

}

type ApiList = typeof apiDetails;
export const apiList: ApiList = apiDetails;